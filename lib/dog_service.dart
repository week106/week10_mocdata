import 'package:dog_app/dog.dart';

var lastId = 3;
var mocDogs = [Dog(id: 1, name: 'A', age: 8), Dog(id: 2, name: 'B', age: 5)];

int getNewId(){
  return lastId++;
}

Future<void> addNewDog(Dog dog){
  return Future.delayed(Duration(seconds: 1), (){
    mocDogs.add(Dog(id: getNewId(), name: dog.name, age: dog.age));
  });
}
Future<void> saveDog(Dog dog){
  return Future.delayed(Duration(seconds: 1), (){
    var index = mocDogs.indexWhere((item) => item.id == dog.id);
    mocDogs[index] = dog;
  });
}
Future<void> delDog(Dog dog){
  return Future.delayed(Duration(seconds: 1), (){
    var index = mocDogs.indexWhere((item) => item.id == dog.id);
    mocDogs.removeAt(index);
  });
}

Future<List<Dog>> getDogs(){
  return Future.delayed(Duration(seconds: 1), () => mocDogs);
}
